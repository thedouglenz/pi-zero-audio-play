#!/bin/python

import time
import os
import signal
import subprocess

import gpiolib as g

SONG_FILENAME = "/home/doug/projects/pi-zero-audio-play/ggrbar.mp3"
BUTTON_IN_PIN = 23

playing = False

process = None


def main():

    global playing

    playing = False

    initialize()

    while True:

        # Block and wait for a rising voltage in the defined input pin
        result = g.gpio.wait_for_edge(BUTTON_IN_PIN, g.gpio.RISING)

        if result is None:
            continue
        else:
            # We detected a rising edge but stop and wait to see if
            # we still have high voltage after 700 ms (long press).
            # Continue loop if it's back to low.
            time.sleep(0.7)
            if not g.is_on(BUTTON_IN_PIN):
                continue
            else:
                on_edge()


def on_edge():
    global playing
    if playing:
        stop_sound()
        playing = False
    else:
        play_sound()
        playing = True


def initialize():
    g.init()
    g.set_pin_in(BUTTON_IN_PIN)


def play_sound():
    global process
    # Call mplayer -msglevel all=-1 wywh.mp
    cmd = ["mpg123", "-q", SONG_FILENAME]
    process = subprocess.Popen(cmd, preexec_fn=os.setsid)


def stop_sound():
    global process
    os.killpg(os.getpgid(process.pid), signal.SIGTERM)


if __name__ == "__main__":
    playing = False
    try:
        main()
    except Exception as e:
        print(str(e))
        stop_sound()
        g.cleanup()
